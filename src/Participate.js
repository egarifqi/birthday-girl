import React from 'react';
import './App.css';
import {
    Box,
    Button,
    Center,
    Container,
    Flex,
    Grid,
    GridItem,
    Input,
    Modal,
    ModalCloseButton,
    ModalContent,
    ModalHeader,
    ModalOverlay,
    Spacer,
    Text,
    Textarea,
    useDisclosure,
    Spinner,
    useToast,
} from '@chakra-ui/react';
import { useNavigate } from 'react-router-dom';
import defaultAvatar from './assets/blank-avatar.png';
import avatar1 from './assets/avatar1.jpg';
import avatar2 from './assets/avatar2.jpg';
import avatar3 from './assets/avatar3.jpg';
import avatar4 from './assets/avatar4.jpg';
import avatar5 from './assets/avatar5.jpg';
import avatar6 from './assets/avatar6.jpg';
import avatar7 from './assets/avatar7.jpg';
import avatar8 from './assets/avatar8.jpg';
import header from './assets/header.png';
import footer from './assets/footer.png';
import qoreContext from './qoreContext';

function Participate() {
    const navigate = useNavigate();
    const toast = useToast();
    const client = qoreContext.useClient();
    const { insertRow } = qoreContext.views.allWishes.useInsertRow();
    const [avatar, setAvatar] = React.useState(defaultAvatar);
    const [fileAvatar, setFileAvatar] = React.useState(null);
    const [isLoading, setIsLoading] = React.useState(false);
    const [input, setInput] = React.useState({
        name: '',
        called: '',
        wishes: '',
        color: '#FFFFFF'
    })
    const [listColor] = React.useState([
        '#D9D7F1',
        '#D6E5FA',
        '#FFC4E1',
        '#D3E4CD',
        '#DEEDF0',
        '#FFE5B9',
        '#B5EAEA',
        '#FFFFFF',
    ]);

    const [listAvatar] = React.useState([
        avatar1,
        avatar2,
        avatar3,
        avatar4,
        avatar5,
        avatar6,
        avatar7,
        avatar8,
    ])

    const {
        onClose: onCloseAvatarChoser,
        onOpen: onOpenAvatarChoser,
        isOpen: isOpenAvatarChoser,
    } = useDisclosure();

    const {
        onClose: onCloseCardChoser,
        onOpen: onOpenCardChoser,
        isOpen: isOpenCardChoser,
    } = useDisclosure();

    const {
        onClose: onClosePreviewCard,
        onOpen: onOpenPreviewCard,
        isOpen: isOpenPreviewCard,
    } = useDisclosure();
    
    const _handleImgChange = (e) => {
        const file = e.currentTarget.files?.item(0);
        const url = URL.createObjectURL(file);
        setFileAvatar(file);
        setAvatar(url);
    }

    const handleChange = (event) => {
        const temp = { ...input };
        temp[event.target.name] = event.target.value;
        setInput(temp);
    }

    const onColorChose = (color) => {
        const temp = { ...input };
        temp.color = color;
        setInput(temp);
        onCloseCardChoser();
    }

    const onAvatarChose = (newAvatar) => {
        setAvatar(newAvatar);
        fetch(newAvatar)
        .then(res => res.blob())
        .then(async (blob) => {
            const file = new File([blob], "avatar image.jpeg", { type: "image/jpeg" })
            setFileAvatar(file)
        })
        const inputAvatar = document.getElementById('avatar-input');
        inputAvatar.value = null;
        onCloseAvatarChoser();
    }

    const isValid = () => {
        if (!input.name || !input.called || !input.wishes || avatar === defaultAvatar) {
            return false;
        }
        return true;
    }
    
    const handleSubmit = async () => {
        setIsLoading(true);
        const validationResponse = isValid();
        
        if (validationResponse) {
            try {
                let url = await client.view('allWishes').upload(fileAvatar);
                 
                const wishesItem = {
                    name: input.name,
                    called: input.called,
                    wishes: input.wishes,
                    color: input.color,
                    avatar: url,
                }
        
                await insertRow(wishesItem);

                navigate('/thank-you')
            } catch (error) {
                toast({
                    title: 'Failed',
                    description: 'Terjadi kesalahan, mohon coba lagi',
                    position: 'top',
                    status: 'error',
                    duration: 3000,
                    isClosable: true,
                });
            }
        } else {
            toast({
                title: 'Failed',
                description: 'Mohon isi lengkap form dan avatar',
                position: 'top',
                status: 'error',
                duration: 3000,
                isClosable: true,
            });
        }
        setIsLoading(false);
    }

    return (
        <>
            <Box
                className="body"
            >
                {isLoading && (
                    <Box
                        width="100vw"
                        height="100vh"
                        className='overlay-loading'
                    >
                        <Spinner
                            size="xl"
                            color="white"
                            className='loading'
                        />
                    </Box>
                )}
                <Container
                    className='main-container'
                >
                    <img
                        alt='header'
                        className='image-header'
                        src={header}
                    />
                    <img
                        alt='footer'
                        className='image-footer'
                        src={footer}
                    />
                    <Center>
                        <Box className='card-container'>
                            <Center>
                                <h1
                                    className='text participate-title'
                                >
                                    Say your wishes!
                                </h1>
                            </Center>
                            <div className='avatar-container'>
                                <img
                                    alt='avatar'
                                    src={avatar}
                                    className='avatar-image'  
                                />
                                <input
                                    id='avatar-input'
                                    type="file"
                                    accept="image/*"
                                    className='avatar-input'
                                    onChange={(e) => _handleImgChange(e, "image")}
                                />
                            </div>
                            <Center className='text'>
                                <strong>or</strong>
                            </Center>
                            <Center
                                style={{
                                    margin: '4px 0 16px 0'
                                }}
                            >
                                <Button
                                    className='main-button text'
                                    onClick={() => onOpenAvatarChoser()}
                                >
                                    Choose Avatar
                                </Button>
                            </Center>
                            <Text className='text'><strong>Your name:</strong></Text>
                            <Input
                                mb={4}
                                value={input.name}
                                name="name"
                                onChange={(e) => handleChange(e)}
                            />

                            <Text className='text'><strong>You called her by:</strong></Text>
                            <Input
                                mb={4}
                                value={input.called}
                                name="called"
                                onChange={(e) => handleChange(e)}
                            />

                            <Text className='text'><strong>Your wishes for her</strong></Text>
                            <Textarea
                                mb={4}
                                value={input.wishes}
                                name="wishes"
                                onChange={(e) => handleChange(e)}
                            />

                            <Text className='text'><strong>Your card color:</strong></Text>
                            <Box
                                onClick={() => onOpenCardChoser()}
                                mb={6}
                                style={{
                                    width: '20%',
                                    height: '32px',
                                    background: input.color,
                                    border: '2px solid rgba(0,0,0,0.6)'
                                }}
                            />

                            <Flex>
                                <Button
                                    className='sub-button text'
                                    onClick={() => onOpenPreviewCard()}
                                >
                                    Preview
                                </Button>
                                <Spacer />
                                <Button
                                    className='main-button text'
                                    onClick={() => handleSubmit()}
                                >
                                    Submit
                                </Button>
                            </Flex>
                        </Box>
                    </Center>

                    <Modal
                        onClose={onCloseAvatarChoser}
                        isOpen={isOpenAvatarChoser}
                        isCentered
                    >
                        <ModalOverlay />
                        <ModalContent
                            padding='0 16px 24px 16px'
                        >
                            <ModalHeader>Choose Avatar</ModalHeader>
                            <ModalCloseButton />
                            <Grid templateColumns='repeat(4, 1fr)' gap={12}>
                                {listAvatar.map((item, index) => (
                                    <GridItem
                                        key={index}
                                        w='48px'
                                        h='48px'
                                        margin='auto'
                                        borderRadius='50%'
                                        onClick={() => onAvatarChose(item)}
                                    >
                                        <img
                                            src={item}
                                            width='100%'
                                            height='100%'
                                            borderRadius='50%'
                                            alt={`avatar-${index}`}
                                        />
                                    </GridItem>
                                ))}
                            </Grid>
                        </ModalContent>
                    </Modal>
                    
                    <Modal
                        onClose={onCloseCardChoser}
                        isOpen={isOpenCardChoser}
                        isCentered
                    >
                        <ModalOverlay />
                        <ModalContent
                            padding='0 16px 24px 16px'
                        >
                            <ModalHeader>Choose Color</ModalHeader>
                            <ModalCloseButton />
                            <Grid templateColumns='repeat(4, 1fr)' gap={12}>
                                {listColor.map((item) => (
                                    <GridItem
                                        key={item}
                                        bg={item}
                                        w='48px'
                                        h='48px'
                                        margin='auto'
                                        borderRadius='50%'
                                        border='1px solid rgba(0,0,0,0.6)'
                                        onClick={() => onColorChose(item)}
                                    />
                                ))}
                            </Grid>
                        </ModalContent>
                    </Modal>

                    <Modal
                        onClose={onClosePreviewCard}
                        isOpen={isOpenPreviewCard}
                        isCentered
                    >
                        <ModalOverlay />
                        <ModalContent
                            padding='0 16px 24px 16px'
                        >
                            <ModalHeader>Your Card</ModalHeader>
                            <ModalCloseButton />
                            <Box
                                className='card-wish'
                                style={{
                                    background: input.color,
                                }}
                            >
                                <Grid templateColumns='repeat(7, 1fr)' gap={4}>
                                    <GridItem colSpan={2}>
                                        <img
                                            src={avatar}
                                            alt='card-avatar'
                                            className='card-avatar'
                                        />
                                    </GridItem>
                                    <GridItem colSpan={5}>
                                        <Text mb={4}>
                                            Dear <strong>{input.called}</strong>,<br />
                                        </Text>
                                        {input.wishes}
                                        <Flex mt={4}>
                                            <Spacer />
                                            From
                                        </Flex>
                                        <Flex>
                                            <Spacer />
                                            <strong>{input.name}</strong>
                                        </Flex>
                                    </GridItem>
                                </Grid>
                            </Box>
                        </ModalContent>
                    </Modal>
                </Container>
            </Box>
        </>
    );
}

export default Participate;
