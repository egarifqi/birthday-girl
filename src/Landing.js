import React from 'react';
import './App.css';
import { useNavigate } from 'react-router-dom';
import DateFormat from 'dateformat';
import { Box, Container, Text, useToast } from '@chakra-ui/react';
import topBox from './assets/top-box.png';
import bottomBox from './assets/bottom-box.png';
import highlight from './assets/white-highlight.png';
import header from './assets/header.png';
import footer from './assets/footer.png';

function Landing() {
  const navigate = useNavigate();
  const toast = useToast();
  const [isBoxOpen, setIsBoxOpen] = React.useState(false);

  const openBox = () => {
    if (DateFormat(new Date(), 'yyyy-mm-dd') === '2022-01-28') {
      setIsBoxOpen(true);
  
      setTimeout(() => {
        navigate('/showcase')
      }, 4000)
    } else {
      toast({
        title: 'Belum waktunya!',
        description: 'Ditunggu sampe tanggal 28 yaa ~',
        position: 'top',
        status: 'error',
        duration: 5000,
        isClosable: true,
      })
    }
  }

  return (
    <Box
      className="body"
    >
      <Container
        className='main-container'
        style={{
            background: `#e4f7f8`
        }}
      >
        <img
            alt='header'
            className='image-header'
            src={header}
        />
        <img
            alt='footer'
            className='image-footer'
            src={footer}
        />
        <div>
          <header className="App-header">
            <div className='box-image' >
              <div
                className={`App-logo ${!isBoxOpen && 'move-animation'}`}
                alt='box'
                style={{
                  cursor: 'pointer'
                }}
                onClick={() => openBox()}
              >
                <img src={topBox} className={`normal ${isBoxOpen && 'open-animation'}`}  alt='top-box' />
                <img src={bottomBox} alt='bottom-box' />
              </div>
              <img
                className='highlight'
                alt='highlight'
                src={highlight}
              />
            </div>
            <Text onClick={() => openBox()} className={`text click-me-text ${!isBoxOpen && 'move-animation'}`}>
              CLICK ME!
            </Text>
          </header>
        </div>
      </Container>
    </Box>
  );
}

export default Landing;
