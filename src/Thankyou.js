import './App.css';
import { Box, Container, Text, Center } from '@chakra-ui/react';
import highlight from './assets/white-highlight.png';
import header from './assets/header.png';
import footer from './assets/footer.png';
import { Link } from 'react-router-dom';

function ThankYou() {
  return (
    <Box
      className="body"
    >
      <Container
        className='main-container'
        style={{
            background: `#e4f7f8`
        }}
      >
        <img
            alt='header'
            className='image-header'
            src={header}
        />
        <img
            alt='footer'
            className='image-footer'
            src={footer}
        />
        <div>
          <header className="App-header">
            <div className='box-image'>
                <Center>
                    <Text className='text click-me-text'>
                        THANK YOU!
                    </Text>
                </Center>
                <Center>
                    <Text className='subtext compress-width'>
                        Hope the good wishes return to you
                    </Text>
                </Center>
                <Center>
                    <Link to='/participate'>
                        <Text className='subtext resubmit' mt={6}>
                            RESUBMIT
                        </Text>
                    </Link>
                </Center>
              <img
                className='highlight'
                alt='highlight'
                src={highlight}
              />
            </div>
          </header>
        </div>
      </Container>
    </Box>
  );
}

export default ThankYou;
