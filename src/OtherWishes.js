import React from 'react';
import './App.css';
import {
    Box,
    Spacer,
    Text,
    Flex,
    Grid,
    GridItem,
} from '@chakra-ui/react';
import Slider from 'react-slick';

function OtherWishes() {
    const settings = {
        dots: true,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        speed: 1000,
        autoplaySpeed: 10000,
        pauseOnHover: true,
    };

    // let { data: wishes } = qoreContext.view('allWishes').useListRow();
    const wishes = [];

    return (
        <Box>
            <Text
                mt={6}
                mb={4}
                className='text title-text'
            >
                Wishes from friends
            </Text>
            {wishes.length > 0 && (
                <Slider {...settings} className='fade-in'>
                    {wishes.map(item => (
                        <div>

                            <Box
                                className='card-wish'
                                style={{
                                    backgroundColor: item.color,
                                }}
                            >
                                <Grid templateColumns='repeat(7, 1fr)' gap={4}>
                                    <GridItem colSpan={2}>
                                        <img
                                            src={item.avatar}
                                            alt='card-avatar'
                                            className='card-avatar'
                                        />
                                    </GridItem>
                                    <GridItem colSpan={5}>
                                        <Text mb={4}>
                                            Dear <strong>{item.called}</strong>,<br />
                                        </Text>
                                        {item.wishes}
                                        <Flex mt={4}>
                                            <Spacer />
                                            From
                                        </Flex>
                                        <Flex>
                                            <Spacer />
                                            <strong>{item.name}</strong>
                                        </Flex>
                                    </GridItem>
                                </Grid>
                            </Box>
                        </div>
                    ))}
                </Slider>
            )}
        </Box>
    );
}

export default OtherWishes;
