import React from 'react';
import {
  BrowserRouter,
  Route,
  Routes,
} from 'react-router-dom';
import Landing from './Landing';
import Participate from './Participate';
import ThankYou from './Thankyou';
import Showcase from './Showcase';

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/showcase" element={<Showcase />} />
        <Route path="/thank-you" element={<ThankYou />} />
        <Route path="/participate" element={<Participate />} />
        <Route path="/" element={<Landing />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
