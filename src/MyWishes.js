import React from 'react';
import './App.css';
import {
    Spacer,
    Text,
    Flex,
    Button,
    Accordion,
    AccordionItem,
    AccordionPanel,
    AccordionButton,
} from '@chakra-ui/react';

function MyWishes() {

    const [isShow, setIsShow] = React.useState(false);

    const showHandler = () => {
        setIsShow(!isShow);
    }

    return (
        <Accordion allowToggle>
            <AccordionItem
                border='none'
            >
                <Flex
                    mt={4}
                >
                    <Text
                        className='text title-text'
                    >
                        My Wishes
                    </Text>
                    <Spacer />
                    <AccordionButton
                        className='accordion-button'
                        onClick={() => showHandler()}
                    >
                        <Button
                            className='main-button text w100'
                        >
                            {isShow ? 'Close' : 'Open'}
                        </Button>
                    </AccordionButton>
                </Flex>
                <AccordionPanel>
                    <Text className='text'>
                        Rahasia
                    </Text>
                </AccordionPanel>
            </AccordionItem>
        </Accordion>
    );
}

export default MyWishes;
