import React from 'react';
import './App.css';
import {
    Box,
    Center,
    Container,
} from '@chakra-ui/react';
import header from './assets/header.png';
import mainImage from './assets/maru-image.png';
import confetti from './assets/confetti.gif';
import birthdayText from './assets/birthday-text.png';
import ReactTyped from 'react-typed';
import MyWishes from './MyWishes';
import OtherWishes from './OtherWishes';

function Participate() {
    const [isNextTypingShow, setIsNextTypingShow] = React.useState(false);
    const [isWishesShow, setIsWishesShow] = React.useState(false);

    React.useEffect(() => {
        setTimeout(() => {
            setIsNextTypingShow(true);
        }, 2000)

        setTimeout(() => {
            setIsWishesShow(true);
        }, 9500)
    }, []);

    return (
        <>
            <Box
                className="body"
            >
                <Container
                    className='main-container'
                    style={{
                        backgroundColor: 'white'
                    }}
                >
                    <img
                        alt='header'
                        className='image-header'
                        src={header}
                    />
                    <img
                        alt='confetti'
                        src={confetti}
                        className='confetti'
                    />
                    <Center>
                        <Box
                            className='fade-in'
                            style={{
                                marginTop: '9em',
                                width: '40%',
                                zIndex: '9'
                            }}
                        >
                            <img
                                alt='maru'
                                src={mainImage}
                                width='100%'
                                style={{
                                    zIndex:'3'
                                }}
                            />
                        </Box>
                    </Center>
                    <Box
                        className='fade-in'
                        style={{
                            zIndex: '9',
                            width: '90%',
                            margin:'-24px auto 0 auto',
                        }}
                    >
                        <img
                            alt='bday'
                            src={birthdayText}
                            style={{
                                zIndex: '3'
                            }}
                        />
                    </Box>
                    <Box
                        style={{
                            borderRadius: '8px 8px 0px 0px',
                            backgroundColor: 'white',
                            marginTop: '-24px',
                            padding: '16px',
                            boxShadow: '-1px -6px 8px rgba(0, 30, 77, 0.2)'
                        }}
                    >
                        <ReactTyped
                            strings={[
                                'Dear <strong>Mar&apos;a</strong>,'
                            ]}
                            typeSpeed={40}
                            className='text'
                        />
                        <br />
                        {isNextTypingShow && (
                            <ReactTyped
                                strings={[
                                    'You have so many wishes from me and your friends. May you happy with this best wishes for you.'
                                ]}
                                typeSpeed={50}
                                className='text'
                            />
                        )}

                        {isWishesShow && (
                            <Box
                                className='fade-in'
                            >
                                <MyWishes />
                                <OtherWishes />
                            </Box>
                        )}
                    </Box>
                </Container>
            </Box>
        </>
    );
}

export default Participate;
